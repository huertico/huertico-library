# Configuration file for Sphinx documentation builder.

import os
import sys

project = "huertico-library"
copyright = "2019, constrict0r, valarauco"
author = "huertico"
version = "0.0.1"
release = "0.0.1"

sys.path.insert(0, os.path.abspath("../.."))

extensions = [
    "sphinxcontrib.restbuilder",
    "sphinxcontrib.globalsubs",
    "sphinx-prompt",
    "sphinx_substitution_extensions"
]

templates_path = ["_templates"]

exclude_patterns = []

html_static_path = ["_static"]

html_theme = "sphinx_rtd_theme"

master_doc = "index"

img_url_base = "https://gitlab.com/"

img_url_part = "/images/raw/master/"

img_url = img_url_base + author + img_url_part + "/" + project + "/"

ci_url_base = "https://gitlab.com/"

ci_url = ci_url_base + author + "/" + project + "/pipelines"

global_substitutions = {
    "AUTHOR_IMG": ".. image:: " + img_url + author +
    ".png\n   :alt: huertico",
    "AUTHOR_SLOGAN": "Huertico - Automated Plant Growing.",
    "CI_BADGE": ".. image:: " + img_url_base + author + "/" + project +
    "/badges/master/pipeline.svg\n   :alt: pipeline",
    "CI_LINK":  "`Gitlab CI <" + ci_url + ">`_.",
    "CLASS_IMG": ".. image:: " + img_url + "/class.png\n   :alt: class",
    "REPO_LINK":  "`Gitlab repository <https://gitlab.com/"
    + author + "/" + project + ">`_.",
    "PROJECT": project
}

substitutions = [
    ("|AUTHOR|", author),
    ("|PROJECT|", project)
]
