
huertico-library
****************

.. image:: https://gitlab.com/huertico/huertico-library/badges/master/pipeline.svg
   :alt: pipeline

Unified Huertico Library.

Source code on `Gitlab repository
<https://gitlab.com/huertico/huertico-library>`_.


Contents
********

* `Description <#Description>`_
* `Usage <#Usage>`_
* `Requirements <#Requirements>`_
* `Compatibility <#Compatibility>`_
* `License <#License>`_
* `Links <#Links>`_
* `UML <#UML>`_
   * `Class <#class>`_
* `Author <#Author>`_

Description
***********

Unified Huertico Library.


Usage
*****


Requirements
************


Compatibility
*************

* ESP8266.


License
*******

GPL 3. See the LICENSE file for more details.


Links
*****

`Gitlab repository <https://gitlab.com/huertico/huertico-library>`_

`Gitlab CI <https://gitlab.com/huertico/huertico-library/pipelines>`_


UML
***


Class
=====

The classes structure used in this project is shown below:

.. image:: https://gitlab.com//huertico//images/raw/master////huertico-library//class.png
   :alt: class


Author
******

.. image:: https://gitlab.com//huertico//images/raw/master////huertico-library//huertico.png
   :alt: huertico

Huertico - Automated Plant Growing.

