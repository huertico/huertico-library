/*
 *  Huertico Unified Component Library
 *
 *  Written by constrict0r and valarauco 2019.
 *
 *  Copyright (C) 2019  constrict0r, valarauco
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _HUERTICO_LIBRARY_H
#define _HUERTICO_LIBRARY_H

#include <Adafruit_Sensor.h>

/** component types */
typedef enum
{
    COMPONENT_TYPE_LIGHT              = SENSOR_TYPE_LIGHT,
    COMPONENT_TYPE_MOISTURE           = (100),
    COMPONENT_TYPE_RELAY              = (101),
    COMPONENT_TYPE_RELATIVE_HUMIDITY  = SENSOR_TYPE_RELATIVE_HUMIDITY,
    COMPONENT_TYPE_AMBIENT_TEMPERATURE= SENSOR_TYPE_AMBIENT_TEMPERATURE,
    COMPONENT_TYPE_VOLTAGE            = SENSOR_TYPE_VOLTAGE,
    COMPONENT_TYPE_CURRENT            = SENSOR_TYPE_CURRENT
} components_type_t;

/* Sensor event (42 bytes) */
/** struct sensor_event_s is used to provide a single sensor event in a 
 *  common format. 
 */
typedef struct
{
    int32_t component_id;   /**< unique component identifier, **/
                            /**  should be same as sensor_id, if it exist */
    union
    {
        uint8_t turn_on;    /**< Indicates to turn on component */
        uint8_t turn_off;   /**< Indicates to turn off component */
    };
} component_event_t;

/* Component details (44 bytes) */
/** struct component_s is used to describe basic information about a specific
 *  component in huertico. 
 */
typedef struct
{
    sensor_t sensor;        /**< Adafruit sensor info  */
    int32_t component_id;   /**< unique component identifier, */
                            /*   should be same as sensor_id, if it exist */
} component_t;

class Huertico_Library : public Adafruit_Sensor {
    public:
        // Constructor(s)
        Huertico_Library() {}
        virtual ~Huertico_Library() {}

        // These must be defined by the subclass
        virtual bool getEvent(component_event_t*) = 0;
        virtual bool doEvent(component_event_t*) = 0;
        virtual void getComponent(component_t*) = 0;
};

#endif
